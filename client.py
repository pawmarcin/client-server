import socket
import json

class Client:
    def __init__(self, ):
        self.host = '127.0.0.1'
        self.port = 65432
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
    def open_connection(self):
        self.socket.connect((self.host, self.port))
        print(f"Connected to server: {self.host}:{self.port}")
        
        while True:
            message = input("Write command (help, uptime, info or stop to close): ")
            self.send_message(message)
            self.receive_response()
            if message.lower() == 'stop':
                break
            
    def send_message(self, message):
        command = {"command": message}
        self.socket.sendall(json.dumps(command).encode())
        
    def receive_response(self):
        data = self.socket.recv(1024)
        response = json.loads(data.decode())
        print(f"Answer: {response['message']}")

    def close(self):
        self.socket.close()

if __name__ == "__main__":
    client = Client()
    client.open_connection()
