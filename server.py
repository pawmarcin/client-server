import socket
import time
import json

class Server:
    def __init__(self):
        self.host = '127.0.0.1'
        self.port = 65432
        self.version = '0.1.0'
        self.build_server_date = '13.05.24'
        self.connection_socket = None
        self.address_client = None
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.start_time = time.time()
        self.running = True

    def open_connection(self):
        self.socket.bind((self.host, self.port))
        self.socket.listen()
        print(f"Server waits on: {self.host}:{self.port}")
        
        while self.running:
            self.connection_socket, self.address_client = self.socket.accept()
            self.handle_connection(self.connection_socket, self.address_client)
                      
    def handle_connection(self, connection_socket, address_client):
        print(f"Connection from {address_client}")
        with connection_socket:
            while True:
                data = connection_socket.recv(1024)
                if not data:
                    break
                command = json.loads(data.decode())
                response = self.process_command(command["command"])
                connection_socket.sendall(json.dumps(response).encode())
                if response["message"] == "Server stopped":
                    self.running = False
                    self.socket.close()
                    break

    def process_command(self, command):
        if command == "help":
            return {"message": "\n help - shows useful commands,\n uptime - shows server time,\n info -server version and start date,\n stop - closes the connection"}
        elif command == "uptime":
            uptime = (time.time() - self.start_time) / 60  
            return {"message": f"Server works from {uptime:.2f} min."}
        elif command == "info":
            return {"message": f"Server version: {self.version} Server works from: {self.build_server_date}"}
        elif command == "stop":
            return {"message": "Server stopped"}
        else:
            return {"message": "Unknown command."}
            

if __name__ == "__main__":
    server = Server()
    server.open_connection()
